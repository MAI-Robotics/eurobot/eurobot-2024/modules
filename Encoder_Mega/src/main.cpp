#include <Arduino.h>
#include <Encoder.h>
#include <math.h>
#include <std_msgs/Int16.h>
#define ENCODER_OPTIMIZE_INTERRUPTS

// left encoder pins
const int LEFT_ENC_A_PHASE = 3;
const int LEFT_ENC_B_PHASE = 2;

// right encoder pins
const int RIGHT_ENC_A_PHASE = 18;
const int RIGHT_ENC_B_PHASE = 19;

const int encoder_minimum = -32768;
const int encoder_maximum = 32767;

const int interval = 30;

std_msgs::Int16 right_wheel_tick_count;
std_msgs::Int16 left_wheel_tick_count;

int currentposition_right;
int currentposition_left;
double distanceTraveledRight;
double distanceTraveledLeft;
double average;
float distanceLeft;
float distanceRight;
float x;
float y;
unsigned long intervall;

const double wheel_radius = 0.046;
const double wheel_base = 0.22;
const int Encoder_CPR = 620;

Encoder encoderleft(3, 2);
Encoder encoderright(19, 18);

int currentX = 0;
int currentY = 0;
int currentPhi = 0;

void readEncoderA()
{
  if (encoderright.read() != currentposition_right)
  {
    currentposition_right = encoderright.read();
  }
}

void readEncoderB()
{
  if (encoderleft.read() != currentposition_left)
  {
    currentposition_left = encoderleft.read();
  }
}

void calcDist()
{
  distanceLeft = (currentposition_left * 2 * M_PI * wheel_radius) / Encoder_CPR - distanceLeft;
  distanceRight = (currentposition_right * 2 * M_PI * wheel_radius) / Encoder_CPR - distanceRight;
}

void calcPos()
{
  int distanceTotal = (distanceLeft + distanceRight) / 2;
  currentPhi = currentPhi + (distanceLeft + distanceRight) / wheel_base;
  currentX = x + distanceTotal * cos(currentPhi * PI / 180);
  currentY = y + distanceTotal * sin(currentPhi * PI / 180);
}

void setup()
{
  Serial.begin(9600);
  pinMode(RIGHT_ENC_A_PHASE, INPUT_PULLUP);
  pinMode(RIGHT_ENC_B_PHASE, INPUT_PULLUP);
  pinMode(LEFT_ENC_A_PHASE, INPUT_PULLUP);
  pinMode(LEFT_ENC_B_PHASE, INPUT_PULLUP);
}

void loop()
{
  readEncoderA();
  readEncoderB();
  calcDist();
  calcPos();

  Serial.println("x:" + String(currentX));
  Serial.println("y: " + String(currentY));
  Serial.println("phi: " + String(currentPhi));

  delay(250);
}
