# Modules

## Some ros commands
Start roscore in one terminal
``roscore``

Start lidar in one terminal attached to one monitor (using rviz)
``roslaunch rplidar_ros view_rplidar_a1.launch``

Start lidar in one terminal without GUI
``roslaunch rplidar_ros rplidar_a1.launch``


### Encoder_Mega

project for encoders

### Motor_Mega

project for motors
