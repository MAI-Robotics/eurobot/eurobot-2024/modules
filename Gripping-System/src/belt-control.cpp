#include "belt-control.h"
#include "pins.h"

const int stepsPerRevolution = 200; //change
const int Time_between_steps = 1000;
bool LimitSwitch = false;
bool clockwise = true;
int position;

void setDir(Direction direction) { 
    if (direction == Clockwise)
    {
        clockwise = true;
        digitalWrite(belt_DIR, HIGH);
    }
    else if (direction == Counterclockwise)
    {
        clockwise = false;
        digitalWrite(belt_DIR, LOW);
    }
    
}

void updatePositon() {
    if (clockwise == true) {
        position++;
    }
    else {
        position--;
    }
}

void doStep(int customSpeed) {
        digitalWrite(belt_STEP, HIGH);
		delayMicroseconds(customSpeed);
		digitalWrite(belt_STEP, LOW);
		delayMicroseconds(customSpeed);
        updatePositon();
}

void homeBelt() {
    setDir(Clockwise);
    while (LimitSwitch == false) {
        doStep(2000);
    }
    setDir(Counterclockwise);
    for(int x = 0; x < 20; x++) {
        doStep(Time_between_steps);
	}
}

void moveToPosition(int newPosition) {
    if (newPosition > position) {//Move down
        setDir(Counterclockwise);
        for (int i = 0; newPosition <= position; i++) {
            doStep(Time_between_steps);
        }  
    }
    if (newPosition < position) { //move up
        setDir(Clockwise);
        for (int i = 0; newPosition >= position; i++) {
            doStep(Time_between_steps);
        }  
    }
}

