#include "servo_drive.h"
#include "logic.h"
#include "pins.h"

int grapper_right;
int grapper_left;

Servo LEFT_SERVO;
Servo RIGHT_SERVO;
Servo MAIN_SERVO;
Servo Grapper_Right;
Servo Grapper_Left;

void moveUp(){
    /*analogWrite(left_PWM, beltDriveSpeed);
    analogWrite(right_PWM, 0);*/
    while((digitalRead(upper_rail_switch)  == HIGH)){
        delay(5);
        stopTimeout();
    }
    /*analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); */
    delay(1000);
}

void moveDown(){
    /*analogWrite(left_PWM, 0);
    analogWrite(right_PWM, beltDriveSpeed);*/
    while(digitalRead(lower_rail_switch)  == HIGH){
        delay(5);
        stopTimeout();
    }
    /*analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0); */
    delay(1000);
}

void slowSlotting() {
    for (int pos = 40; pos >= 1; pos -= 1) { 
        Grapper_Left.write(pos);              
        delay(15);                       
    }
    delay(1000);
    for (int pos = 130; pos <= 180; pos += 1) { 
        Grapper_Right.write(pos);              
        delay(15);                       
    }    
}

void setAngle(bool ok, int angle) {
    if (ok == true) {
        Grapper_Left.write(angle);
    }
    else {
        Grapper_Right.write(angle);
    }
    
}

void collecting() {
    Grapper_Left.write(40);
    delay(1000);
    Grapper_Right.write(130);
}

void slotted() {
    Grapper_Left.write(1);
    delay(1000);
    Grapper_Right.write(180);
}

void idle() {
    Grapper_Right.write(1);
    delay(1000);
    Grapper_Left.write(180);
}

void liftArm(){
    stopTimeout();
    MAIN_SERVO.write(MAIN_SERVO_LIFT);
}

void lowerArm(){
    stopTimeout();
    MAIN_SERVO.write(MAIN_SERVO_LOWER);
}

void openGripperBasket(){
    stopTimeout();
    LEFT_SERVO.write(LEFT_SERVO_OPENBASKET);
    RIGHT_SERVO.write(RIGHT_SERVO_OPENBASKET);
}

void openGripper(){
    stopTimeout();
    LEFT_SERVO.write(LEFT_SERVO_OPEN);
    RIGHT_SERVO.write(RIGHT_SERVO_OPEN);
}

void closeGripper(){
    stopTimeout();
    LEFT_SERVO.write(LEFT_SERVO_CLOSE);
    RIGHT_SERVO.write(RIGHT_SERVO_CLOSE);
}

void detachServos() {
    LEFT_SERVO.detach();
    RIGHT_SERVO.detach();
    MAIN_SERVO.detach();
}