#include "logic.h"
#include "servo_drive.h"
#include "stepper_drive.h"
#include "pins.h"
#include "communication.h"


float speed = 100;          //Speed, from 0% to 100%
float minTperStep = 540;    //min time per step depens on the voltage
float maxTperStep = 1500;   // tPerStep percentage = 0%

bool CoAEnabled = CoADefault;

IgnoredUS ignoredUSdefault = NONE;

bool enemyDetected = false;
unsigned long currentMillis = millis();
float millisStarted;

void startUpRoutine(bool waitPullcord){
    setPins();
    //moveUp();   //moves gripper to top of bot
    //lowerArm();
    //openGripper();
    //delay(500);

    // calibrationRoutine();    

    /*if(digitalRead(team_select) == HIGH) {  // run once, if high switch is pressed at O
      team = Green;
    } else if(digitalRead(team_select) == LOW) {
      team = Blue;  // every turning will be reversed, whole driving will be mirrored
    }

    if(waitPullcord) waitForPullcord();*/
}

void waitForPullcord() {
    digitalWrite(25,HIGH);  //led on
    while(digitalRead(pullcord)  == LOW){
        delay(5);
    }
    digitalWrite(25,LOW);   //led off

    currentMillis = millis();
    millisStarted = currentMillis / 1000;                           //seconds when pulled pullcord
    DEBUG && Serial.print("Time passed until started: ");
    DEBUG && Serial.println(millisStarted);

    // start
}

void endingRoutine() {
    CoAEnabled = true;
    // driveTo(400, 500, true);
    driveTo(endPosX, endPosY, true);    // x5
    liftArm();
    turnTo(270);
    driveMaxUntilSwitch(true, false, false);
}

void holderRoutine(int setX, int setY, float setDir) {
    driveDistanceMM(driveFurtherMM, 0, true, No, CoAEnabled);
    delay(500);
    driveDistanceMM(holderDriveBackMM, 0, false, No, CoAEnabled);   //180°
    setPos(setX, setY, setDir);

    // openGripperBasket();
    lowerArm();
    openGripper();
    moveDown();
    delay(500);
    closeGripper();
    delay(500);
    moveUp();

    driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);
    liftArm();
}

void preBasketRoutine() {
    // closeGripper();
    // moveUp();

    liftArm();
    CoAEnabled = false;
    delay(500);
}

void basketRoutine(int setX, int setY, float setDir) {
    driveDistanceMM(driveFurtherMM, 0, true, No, CoAEnabled);
    openGripperBasket();
    delay(2000);
    closeGripper();
    setPos(setX, setY, setDir);
}

void afterBasketRoutine() {
    driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);
    
    openGripper();
    lowerArm();
    CoAEnabled = CoADefault;
    delay(500);
}

void homingRoutine(int startPos, bool enabled) {
    int xValue;
    int yValue;

    CoAEnabled = false;

    if(enabled) {
        switch(startPos) {
            case 1:         //team green final zone
                liftArm();
                turnAngle(90, false); 
                delay(500);
                driveMaxUntilSwitch(true, false);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                turnAngle(90, false);
                delay(500);
                driveMaxUntilSwitch(true, false, true);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                lowerArm();
                break;
            case 2:
                turnAngle(90, false);
                delay(500);
                driveMaxUntilSwitch(true, false, true);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                break;
            case 3:
                driveMaxUntilSwitch(true, false, true);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                break;
            case 4:
                driveMaxUntilSwitch(true, false);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                turnAngle(90, true);
                delay(500);
                driveMaxUntilSwitch(true, false, true);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                break;
            case 5:
                turnAngle(90, true);
                delay(500);
                driveMaxUntilSwitch(true, false, true);
                driveDistanceMM(distanceHoming, 0, false, No, false);
                break;
        }
    }
    
    //turning and setting pos afterwards
    switch(startPos) {
        case 1:
            (enabled?turnAngle(90, false):turnAngle(90, true)); //angle 0
            xValue = 0 + distanceHoming + botLenght/2;
            yValue = 0 + distanceHoming + botLenght/2;
            break;
        case 2:
            (enabled?turnAngle(180, false):turnAngle(90, true)); //angle 0
            xValue = 0 + distanceHoming + botLenght/2;
            yValue = 1850;
            break;
        case 3:
            (enabled?turnAngle(90, true):turnAngle(90, true));
            xValue = 700;
            yValue = 3000 - distanceHoming - botLenght/2;
            break;
        case 4:
            if(!enabled) turnAngle(90, true);
            xValue = 2000 - distanceHoming - botLenght/2;
            yValue = 3000 - distanceHoming - botLenght/2;
            break;
        case 5:
            if(!enabled) turnAngle(90, true);
            xValue = 2000 - distanceHoming - botLenght/2;
            yValue = 1120;
            break;
    }

    setPos(xValue, yValue, 0);

    delay(1000);
    //bot ends turned to 0 (right)
}

void stopTimeout() {
  currentMillis = millis();   //if time runs out, 
  unsigned long currentSeconds = (currentMillis / 1000) - millisStarted;
  if(currentSeconds >= (maxPlaytime - timeDriveBack)) {                       //time to drive home! (only if enemy detected, obv)
    detachServos();
    /*analogWrite(left_PWM, 0);
    analogWrite(right_PWM, 0);*/
    while (true) {  
      digitalWrite(25, LOW);    
      delay(500);                                                     //dont do anything else if driven home
      digitalWrite(25, HIGH);
      delay(500);
    }
  }
}

void detectEnemy(UsedUS usedUS, bool timeAutoStop, IgnoredUS ignoredUS){
    do {
        const int NUM_US_SENSORS = 9+1;
        const int CoA_LEFT = 0;
        const int CoA_LEFT_FRONT = 1;   //used
        const int CoA_LEFT_BACK = 2;
        const int CoA_RIGHT = 3;
        const int CoA_RIGHT_FRONT = 4;  //used
        const int CoA_RIGHT_BACK = 5;
        const int CoA_FRONT_LEFT = 6;   //used
        const int CoA_FRONT_RIGHT = 7;
        const int CoA_BACK_LEFT = 8;
        const int CoA_BACK_RIGHT = 9;   //used

        enemyDetected = false;
        bool usDetected[NUM_US_SENSORS] = {false};                  // no us sensor detected by default


        for (int i = 0; i < NUM_US_SENSORS; i++) {
            if (filterZero ? (usValues[i] < minCoAregisterValue && usValues[i] > 0) : (usValues[i] < minCoAregisterValue)) {
                usDetected[i] = true;
            }
        }

        if (team == Green) {
            switch(ignoredUS) {
                case NONE:
                    break;
                case L:
                    usDetected[CoA_LEFT] = false;
                    usDetected[CoA_LEFT_FRONT] = false;
                    usDetected[CoA_LEFT_BACK] = false;
                    break;
                case R:
                    usDetected[CoA_RIGHT] = false;
                    usDetected[CoA_RIGHT_FRONT] = false;
                    usDetected[CoA_RIGHT_BACK] = false;
                    break;
                case F:
                    usDetected[CoA_FRONT_LEFT] = false;
                    usDetected[CoA_FRONT_RIGHT] = false;
                    break;
                case B:
                    usDetected[CoA_BACK_LEFT] = false;
                    usDetected[CoA_LEFT_BACK] = false;
                    break;
            }
        }
        else if (team == Blue)
        {
            switch(ignoredUS) {
            case NONE:
                break;
            case R:
                usDetected[CoA_LEFT] = false;
                usDetected[CoA_LEFT_FRONT] = false;
                usDetected[CoA_LEFT_BACK] = false;
                break;
            case L:
                usDetected[CoA_RIGHT] = false;
                usDetected[CoA_RIGHT_FRONT] = false;
                usDetected[CoA_RIGHT_BACK] = false;
                break;
            case F:
                usDetected[CoA_FRONT_LEFT] = false;
                usDetected[CoA_FRONT_RIGHT] = false;
                break;
            case B:
                usDetected[CoA_BACK_LEFT] = false;
                usDetected[CoA_LEFT_BACK] = false;
                break;
            }
        }


        if((usDetected[CoA_LEFT] && (usedUS == ALL || usedUS == LEFT || usedUS == FW || usedUS == BW))    ||        //only front left, left and right sensor to avoid getting slow values because of PCBs
        (usDetected[CoA_RIGHT] && (usedUS == ALL || usedUS == RIGHT || usedUS == FW || usedUS == BW))     ||
        (usDetected[CoA_BACK_RIGHT] && (usedUS == ALL || usedUS == BW))   ||
        (usDetected[CoA_FRONT_LEFT] && (usedUS == ALL || usedUS == FW)))  {

        // if ((usDetected[CoA_LEFT] && usDetected[CoA_LEFT_FRONT]) && (usedUS == FW || usedUS == ALL || usedUS == LEFT)     ||   //left and left front
        // (usDetected[CoA_LEFT] && usDetected[CoA_LEFT_BACK]) && (usedUS == BW || usedUS == ALL || usedUS == LEFT)          ||   //left and left back
        // (usDetected[CoA_FRONT_LEFT] && usDetected[CoA_FRONT_RIGHT]) && (usedUS == FW || usedUS == ALL)                    ||   //front left and front right
        // (usDetected[CoA_RIGHT] && usDetected[CoA_RIGHT_FRONT]) && (usedUS == FW || usedUS == ALL || usedUS == RIGHT)      ||   //right and right front
        // (usDetected[CoA_RIGHT] && usDetected[CoA_RIGHT_BACK]) && (usedUS == BW || usedUS == ALL || usedUS == RIGHT)       ||   //right and right back
        // (usDetected[CoA_BACK_LEFT] && usDetected[CoA_BACK_RIGHT]) && (usedUS == BW || usedUS == ALL)                      ||   //back left and back right
        // (usDetected[CoA_LEFT_FRONT] && usDetected[CoA_FRONT_LEFT]) && (usedUS == FW || usedUS == ALL)                     ||   //left front and front left
        // (usDetected[CoA_RIGHT_FRONT] && usDetected[CoA_FRONT_RIGHT]) && (usedUS == FW || usedUS == ALL)                   ||   //right front and front right
        // (usDetected[CoA_BACK_LEFT] && usDetected[CoA_LEFT_BACK]) && (usedUS == BW || usedUS == ALL)                       ||   //back left and left back
        // (usDetected[CoA_BACK_RIGHT] && usDetected[CoA_RIGHT_BACK]) && (usedUS == BW || usedUS == ALL)) {     //back right and right back 

            enemyDetected = true;                                                       //to repeat loop instead of running once
            currentMillis = millis();                                                   //current time passed
            unsigned long currentSeconds = (currentMillis / 1000) - millisStarted;      //conv to sec, since start
            // DEBUG && Serial.println("Enemy detected!");

            // DEBUG && Serial.print("Current time passed in seconds since start: ");
            // DEBUG && Serial.println(currentSeconds);                                    //debug current timings

            if(timeAutoStop) stopTimeout();                                                //stop after time runs out

            digitalWrite(25, HIGH);
            delay(2500);

        } else {
            enemyDetected = false;                                                      //no enemy detected
            digitalWrite(25, LOW);
        }
    } while(enemyDetected);                                                             //repeat again if enemy detected
    

}

void setPins() {
    //limit switch setup
    pinMode(lower_rail_switch, INPUT_PULLUP);
    pinMode(upper_rail_switch, INPUT_PULLUP);
    pinMode(left_limit_switch, INPUT_PULLUP);
    pinMode(right_limit_switch, INPUT_PULLUP);
    pinMode(pullcord, INPUT_PULLUP);
    pinMode(team_select, INPUT_PULLUP);
    pinMode(belt_STEP, OUTPUT);
	pinMode(belt_DIR, OUTPUT);
     pinMode(25, OUTPUT);

    //dc motor setup
    /*pinMode(right_PWM, OUTPUT);
    pinMode(left_PWM, OUTPUT);*/

    //stepper motor setup
    pinMode(right_stepper_DIR, OUTPUT);
    pinMode(right_stepper_STEP, OUTPUT);
    pinMode(left_stepper_DIR, OUTPUT);
    pinMode(left_stepper_STEP, OUTPUT);

    //servo setup
    /*LEFT_SERVO.attach(left_servo, 800, 2200);
    RIGHT_SERVO.attach(right_servo, 800, 2200);
    MAIN_SERVO.attach(center_servo, 800, 2200);*/
    Grapper_Left.attach(Grapper_left_servo, 800, 2200);
    Grapper_Right.attach(Grapper_right_servo, 800, 2200);

    pinMode(25, OUTPUT);  //LED
}