#ifndef BELT_H
#define BELT_H
//begin of .h file

#include "globals.h" 

extern bool LimitSwitch;
extern bool clockwise;
extern int position;
enum Direction {Clockwise,Counterclockwise};
void setDir(Direction direction = Clockwise);
void updatePosition();
void doStep();
void homeBelt();
void moveToPosition();

#endif