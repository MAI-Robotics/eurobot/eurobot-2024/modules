#ifndef SERVO_H
#define SERVO_H
//begin of .h file

#include "globals.h" 

const int beltDriveSpeed = 100;                              //always tested with 45

#define MAIN_SERVO_LOWER 0
#define MAIN_SERVO_LIFT 120

#define LEFT_SERVO_OPENBASKET 60                            //values for dropping
#define RIGHT_SERVO_OPENBASKET 120

#define LEFT_SERVO_OPEN 70
#define RIGHT_SERVO_OPEN 180-LEFT_SERVO_OPEN                //deposit before

#define LEFT_SERVO_CLOSE 100                                //values for gripping
#define RIGHT_SERVO_CLOSE 180-LEFT_SERVO_CLOSE //15

extern Servo LEFT_SERVO;
extern Servo RIGHT_SERVO;
extern Servo MAIN_SERVO;
extern Servo Grapper_Right;
extern Servo Grapper_Left;

/**
 * @description: whole gripper moves up to the top of the bot
*/
void moveUp();

/**
 * @description: whole gripper moves down to the bottom of the bot
*/
void moveDown();

void collecting();

void slotted();

void idle();

void slowSlotting();

void setAngle(bool ok, int angle);
/**
 * @description: whole gripper arm gets lifted
*/
void liftArm();

/**
 * @description: whole gripper arm gets lowered
*/
void lowerArm();

/**
 * @description: gripper opens wide
*/
void openGripperBasket();

/**
 * @description: gripper opens narrow
*/
void openGripper();

/**
 * @description: gripper closes to grip cherries
*/
void closeGripper();

void detachServos();

#endif