#include "stepper_drive.h"
#include "pins.h"
#include "communication.h"
#include "logic.h"


float distancePer360Rotation = 590.75; //mm

bool stop_driving = false;
bool teamDir;
Team team;

float x_pos = 0.0;      //current x pos
float y_pos = 0.0;      //current y pos
float direction = 0.0;  //current angle
 
void driveTo(float angle, float distance, bool CoA, Condition condition) {
  float turn = angle-direction; //angle to turn
  DEBUG && Serial.print("Turn: " + String(turn) + " Distance: " + String(distance) + "\n");
  
  directRotation(&turn);

  turnAngle((turn>=0.0?turn:-turn), (turn>=0.0?false:true));
  direction = angle;

  driveDistanceMM((unsigned int)(distance+0.5), 0, true, condition, CoA);
}
 
 
void driveTo(int x, int y, bool CoA, Condition condition) {
  float deltaX = x - x_pos;
  float deltaY = y - y_pos;
  float distance = sqrt((deltaX*deltaX) + (deltaY*deltaY));
  float angle = 0.0;
 
  if (deltaX == 0) {
    if (deltaY > 0) {
      angle = 90.0;
    } else if (deltaY < 0) {
      angle = -90.0;
    }
  } else {
    angle = atan2(deltaY,deltaX) * 180/PI;
  }


  driveTo(angle, distance, CoA, condition);
  return;
}

void turnTo(float angle) {
  float turn = angle-direction; //angle to turn

  directRotation(&turn);

  turnAngle((turn>=0.0?turn:-turn), (turn>=0.0?false:true), true);
  direction = angle;

}

void setPos(int x, int y, float newDirection) {
  x_pos = x;      //current x pos
  y_pos = y;      //current y pos
  direction = newDirection;
}

void resetPos() {
  setPos(0, 0);
}

void oneStepBoth(int timePerStep, bool dir, bool enableTrackDistance) {
  stopTimeout();

  digitalWrite(right_stepper_STEP, HIGH);
  digitalWrite(left_stepper_STEP, HIGH);
  delayMicroseconds(timePerStep);
  digitalWrite(right_stepper_STEP, LOW);
  digitalWrite(left_stepper_STEP, LOW);
  delayMicroseconds(timePerStep);

  if(enableTrackDistance) trackDistance(dir);
}

void trackDistance(bool dir) {
  x_pos = (dir ? x_pos + distancePerStep * cos(direction*PI/180) : x_pos - distancePerStep * cos(direction*PI/180));
  y_pos = (dir ? y_pos + distancePerStep * sin(direction*PI/180) : y_pos - distancePerStep * sin(direction*PI/180));
}

void driveFast(unsigned int steps, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    for (unsigned int i = 0; i < steps; i++)
    {
        //getSensorValues(); -> now on second core
        if(CoA) detectEnemy(dir ? FW : BW);

        //do a step
        oneStepBoth(tPerStep, dir, true);

    }
    delay(10);
}


void driveUntilLine(unsigned int steps, unsigned int distanceOffset, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0;

    for (unsigned int i = 0; i < steps; i++){
        //getSensorValues(); -> now on second core
        if(CoA) detectEnemy(dir ? FW : BW);

        a = 0;

        if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
            a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
        }

        if (steps - i < 300 && i > steps / 2){  // decelerate to make stopping smoother
            a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
        }


        if((irValues[0] < 900) || (irValues[1] < 900) ||(irValues[2] < 900) || (irValues[3] < 900) || (irValues[4] < 900) || (irValues[5] < 900) && (irValues[0] > 0) || (irValues[1] > 0) ||(irValues[2] > 0) || (irValues[3] > 0) || (irValues[4] > 0) || (irValues[5] > 0)){    //line detected but value not 0
          int stepsOffset = distanceOffset / outerWheelDistance * stepsPerRev;
          
          float a = 0;

          for (unsigned int i = 0; i < stepsOffset; i++){
              a = 0;

              if (steps - i < 300 && i > steps / 2){  // decelerate to make stopping smoother
                  a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
              }

              oneStepBoth(tPerStep + a + 100, dir, true);
          }
          break;
        }
        //do a step
        oneStepBoth(tPerStep + a, dir, true);
    }
    delay(10);
}


void driveMaxUntilLine(unsigned int distanceOffset, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);    //set dir
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);      //short delay

    float a = 0;

    while((irValues[0] >= 900) && (irValues[1] >= 900) && (irValues[2] >= 900) && (irValues[3] >= 900) && (irValues[4] >= 900) && (irValues[5] >= 900)) { // no line detected

    //line not detected -> drive
        //getSensorValues(); -> now on second core
        if(CoA) detectEnemy(dir ? FW : BW);
  
        //do a step
        oneStepBoth(tPerStep + a, dir, true);
    } //end of while

    delay(10);

    if(distanceOffset > 0) {      //distance offset
      driveDistanceMM(distanceOffset, 0, dir, No, CoA);
    }
}


void driveUntilSwitch(unsigned int steps, bool dir, bool CoA){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0;

    for (unsigned int i = 0; i < steps; i++)
    {
        //getSensorValues(); -> now on second core
        a = 0;
        if(CoA) detectEnemy(dir ? FW : BW);

        if (i < 300 && i <= steps / 2) {      // accelerate until maximum speed to make starting smoother
            a = tPerStep * 0.0025 * (300 - i);  // adapts the step time
        }

        if (steps - i < 300 && i > steps / 2){  // decelerate to make stopping smoother
            a = tPerStep * 0.0025 * (300 - (steps - i)); // adapts the step time
        }

        if((digitalRead(left_limit_switch)  == LOW) && (digitalRead(right_limit_switch)  == LOW)){
          break;
        }
        //do a step
        oneStepBoth(tPerStep + a, dir, true);
    }
    delay(10);
}


void driveMaxUntilSwitch(bool dir, bool CoA, bool driveFurther){         
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 0 : 1);

    delay(10);

    float a = 0;

    while((digitalRead(left_limit_switch)  == HIGH) && (digitalRead(right_limit_switch)  == HIGH)) {  // no switch pressed
        //getSensorValues(); -> now on second core
        a = 0;
        if(CoA) detectEnemy(dir ? FW : BW);

        //do a step
        oneStepBoth(tPerStep + a, dir, true);
    }
    delay(10);

    if(driveFurther) driveDistanceMM(driveFurtherMM, 0, true, No, CoAEnabled);
}


void driveDistanceMM(unsigned int distance, unsigned int distanceOffset, bool dir, Condition condition, bool CoA){         
    int steps = distance / outerWheelDistance * stepsPerRev;

    switch(condition){
      case Switch:
        driveUntilSwitch(steps, dir, CoA);
        break;
      case Line:
        driveUntilLine(steps, distanceOffset, dir, CoA);
        break;
      case No:
        driveFast(steps, dir, CoA);
        break;
    }
}

bool readTeam(bool oldDir) {
  if(team == Green) {
    teamDir = oldDir;
  } else if(team == Blue) {
    teamDir = !oldDir;
  }

  return teamDir;
}

void turnFast(unsigned int steps, unsigned int distanceOffset, bool dir){  
    digitalWrite(left_stepper_DIR, dir ? 0 : 1);        
    digitalWrite(right_stepper_DIR, dir ? 1 : 0);

    delay(10);

    for (unsigned int i = 0; i < steps; i++)
    {
        //do a step
        oneStepBoth(tPerStep, dir, false);
    }

    if(distanceOffset > 0) {
      driveDistanceMM(distanceOffset, 0, true, No, true);
    }
  delay(100);
}


void turnAngle(float degree, bool dir, bool teamLogic){
  if(teamLogic) {
    dir = readTeam(dir);  //teamgreen does not change dir, teamBlue will reverse dir
  }

  int steps = round(degree/360 *distancePer360Rotation / outerWheelDistance * stepsPerRev);
  
  direction = (int)(dir ? direction - degree : direction + degree) % 360; //x-y system, new dir to turn will be used for x-y system, shorter way will be used

  turnFast(steps, 0, dir);
}

void calibrationRoutine(){
    turnAngle(20, true, false);
    delay(500);

    while(millis() < 18000){
        turnAngle(40, false, false);
        delay(500);
        turnAngle(40, true, false);
        delay(500);
    }
    turnAngle(20, false, false);
}


void followLine(int maxDistance) {
    while(distanceDriven <= maxDistance) {
        left_detected = false;
        center_detected = false;
        right_detected = false;

        //needs updated values every run
        if (irValues[0] < 900 || irValues[1] < 900) {
            left_detected = true;
        }
        else if (irValues[2] < 900 || irValues[3] < 900) {
            center_detected = true;
        }
        else if (irValues[4] < 900 || irValues[5] < 900) {
            right_detected = true;
        }

        if (center_detected) {              // line center detected
            DEBUG && Serial.println("CENTER -> STRAIGT AHAID");

            driveDistanceMM(10, 0, true, No, CoAEnabled);
            distanceDriven += 10;

        } else if (left_detected    && !right_detected) {  // line is left
            DEBUG && Serial.println("LEFT -> TURN LEFT");

            turnAngle(lineFollowAngle, false, false); // links abbiegen
            driveDistanceMM(10, 0, true, No, CoAEnabled);
            distanceDriven += 10;

        } else if (!left_detected && right_detected) {  // line is right
            DEBUG && Serial.println("RIGHT -> TURN RIGHT");

            turnAngle(lineFollowAngle, true, false); // rechts abbiegen
            driveDistanceMM(10, 0, true, No, CoAEnabled);
            distanceDriven += 10;

        } else { 
            DEBUG && Serial.println("LOST LINE -------------");
            driveDistanceMM(10, 0, false, No, CoAEnabled);
            distanceDriven -= 10;
        }
    }
    distanceDriven = 0;
}


void followLineMax() {
    while((irValues[0] < 900) || (irValues[1] < 900) ||(irValues[2] < 900) || (irValues[3] < 900) || (irValues[4] < 900) || (irValues[5] < 900)) {
        left_detected = false;
        center_detected = false;
        right_detected = false;

        //needs updated values every run
        if (irValues[0] < 900 || irValues[1] < 900) {
            left_detected = true;
        }
        else if (irValues[2] < 900 || irValues[3] < 900) {
            center_detected = true;
        }
        else if (irValues[4] < 900 || irValues[5] < 900) {
            right_detected = true;
        }

        if (center_detected) {              // line center detected
            DEBUG && Serial.println("CENTER -> STRAIGT AHAID");

            driveDistanceMM(10, 0, true, No, CoAEnabled);

        } else if (left_detected    && !right_detected) {  // line is left
            DEBUG && Serial.println("LEFT -> TURN LEFT");

            turnAngle(lineFollowAngle, false, false); // links abbiegen
            driveDistanceMM(10, 0, true, No, CoAEnabled);

        } else if (!left_detected && right_detected) {  // line is right
            DEBUG && Serial.println("RIGHT -> TURN RIGHT");

            turnAngle(lineFollowAngle, true, false); // rechts abbiegen
            driveDistanceMM(10, 0, true, No, CoAEnabled);
        } 
    }
}

void directRotation(float* angle) {
  while(*angle >= (float)180.0) {
    *angle -= (float)360.0;
  }
  while(*angle <= (float)-180.0) {
    *angle += (float)360.0;
  }
}

int fixSetPos(int valueToChange, unsigned int arenaDir) {
  int newValue = valueToChange;
  switch (arenaDir) {     // valueToChange is x-value
    case 0:
      newValue = valueToChange - botLenght/2;
      break;

    case 90:            // valueToChange is y-value
      newValue = valueToChange - botLenght/2;
      break;

    case 180:           // valueToChange is x-value
      newValue = valueToChange + botLenght/2;
      break;

    case 270:           // valueToChange is y-value
      newValue = valueToChange + botLenght/2;
      break;
  }
  return newValue;
}
