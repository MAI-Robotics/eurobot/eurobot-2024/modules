#ifndef LOGIC_H
#define LOGIC_H
//begin of .h file

#include "globals.h"
#include "stepper_drive.h"

const String softwareVersion = "v2.0.0";

// Settings file - customize here

const bool DEBUG = false;            // toggle debug messages in serial monitor

extern bool CoAEnabled;             // dont use, will be used for changing CoA setting during the match
const bool CoADefault = true;       // toggle CoA in general
enum UsedUS {FW, BW, LEFT, RIGHT, ALL};
enum IgnoredUS {L, R, F, B, NONE};
extern IgnoredUS ignoredUSdefault;

const bool filterZero = true;       // toggle counting "0" values from US sensors in enemy detection 
const int minCoAregisterValue = 30; // if <Xcm, enemy detected

const int maxPlaytime = 100;        // max match time
const int timeDriveBack = 7;        // drive home x seconds before finish if CoA detected
const int endPosX = 150;            // end pos to drive home if CoA is detected, after that we drive until wall
const int endPosY = 225; 

const int botLenght = 200;          // bot lenght in mm, used when driving until switch, pos is not =0, pos is half of bots lenght! we measure current coordinate from bot middle point
const int driveFurtherMM = 100;     // bot drives X mm furter if driving until wall, holder or basket
const int holderDriveBackMM = 18;   // bot drives X mm back before moving gripper down

const int distanceDriveBackHolder = 70;    //distance to drive back from wall to stand perfectly after turning at distance
const int distanceDriveBack = 125;   // distance to drive back before lifting gripper (at basket etc.)
const int distanceHoming = 125;     // distance to drive back while homing (from side)

const int onLineOffset = 100;       // distance offset to stand on line
const float lineFollowAngle = 5;    // angle to turn while following line

const bool customSpeed = false;            //if true, you can set a custom speed

extern bool enemyDetected;
extern float millisStarted;         // ms passed when pullcord pulled
extern unsigned long currentMillis;

extern float speed;                 //Speed in %, 1 is the slowest, 100% being the highest
extern float minTperStep;           //min time per step, sets the 100% speed
extern float maxTperStep;

extern float direction;

/**
 * @description: startuproutine, can be used for preloading or normal tactics
*/
void startUpRoutine(bool waitPullcord = true);

/**
 * @description: waits until pulling out the pullcord and tracks time to drive back
*/
void waitForPullcord();

/**
 * @description: drives into the final zone
*/
void endingRoutine();

/**
 * @description: grips cherries, moves up -> prepair for basket
*/
void holderRoutine(int setX, int setY, float setDir = direction);

/**
 * @description: lifts arm; run one action before driving to the basket
*/
void preBasketRoutine();

/**
 * @description: drops and grips
*/
void basketRoutine(int setX, int setY, float setDir = direction);

/**
 * @description: lowers arm, prepairs for cherries
*/
void afterBasketRoutine();

/**
 * @description: homing routine, bot starts at same pos every time, start with bot looking from basket away
 * @param startPos: from start zone (1) to plate on enemys side (5)
*/
void homingRoutine(int startPos, bool enabled = true);

/**
 * @description: if time out, wait and dont move anymore
*/
void stopTimeout();

/**
 * @description: enemy detection to run on every step
 * @param trackedUS: us to be used while detecting enemies (FW, BW, ALL)
 * @param driveBack: option to toggle driving into end zone on match end
*/
void detectEnemy(UsedUS trackedUS, bool driveBack = true, IgnoredUS ignoredUS = ignoredUSdefault);

void setPins();

#endif