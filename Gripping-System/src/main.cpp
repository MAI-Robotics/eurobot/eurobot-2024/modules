#include "main.h"


void setup(){
    Serial.begin(9600);
    Serial1.begin(115200);

    DEBUG && Serial.println("START BOT " + softwareVersion);

    startUpRoutine(false);     // pins, gripper, waiting for pullcord (working)
}

void loop(){
  /**
  * Working tactics:
  * defaultTactics();
  * homeZoneTactics();
  * preloadingTactics();
  * 
  * Not finally tested:
  * riskyTactics();
  */

  //preloadingTactics(false);
  
  //detachServos();
  /*collecting();
  delay(10000);
  slotted();
  delay(10000);*/
  /*slotted();
  driveDistanceMM(20, 0, true, condition, false);
  collecting();*/
  driveDistanceMM(400, 0, true, No, false);
  delay(10000);
  driveDistanceMM(400, 0, true, No, false);
  

  while(true) {
    /*setAngle(true, 180);
    setAngle(false, 0);
    delay(10000);
    setAngle(true, 105);
    setAngle(false, 75);
    delay(10000);*/
//right servo default:180-90
  }
}

void setup1(){
  // code goes here
}

/*void loop1(){ // only for receiving data
  Serial1.readStringUntil('\n').toCharArray(inputString, STRING_SIZE); //for deactivating the enemy detection
  
  // Serial.println(inputString);
  getSensorValues();  // us and ir values
  DEBUG && Serial.print("X: " + String(x_pos) + " Y: " + String(y_pos) + " DIR: " + String(direction) + "\n");
}*/


/**
*   Tactics 
*/

void defaultTactics() {   // normal, working
  /*
  * start with bot looking from basket away
  * test code for team green, blue will be reversed
  */

  homingRoutine(2);

  waitForPullcord();
  
  driveTo(250, 1470);   // x2


  turnTo(180);          // x3                 // test with turnTo(), then use
  CoAEnabled = false;
  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(fixSetPos(53, 180), 1500, 180);                     // Holder, working

  CoAEnabled = CoADefault;

  driveTo(700, 400);    // x4

  turnTo(180);          // x5
  CoAEnabled = false;
  driveMaxUntilSwitch(true, CoAEnabled, true);                // 180°
  setPos(fixSetPos(0, 180), 300, 180);                        // test with default option instead of "0"
  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);

  turnTo(270);          // x7
  preBasketRoutine();
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  afterBasketRoutine();

  driveTo(700, distanceDriveBackHolder+botLenght/2);

  turnTo(0);

  CoAEnabled = CoADefault;

  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(985, 150, 0);                 // Holder

  turnTo(180);

  CoAEnabled = false;

  driveMaxUntilSwitch(true, CoAEnabled, true);
  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);

  turnTo(270);          // x11

  preBasketRoutine();
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  afterBasketRoutine();

  endingRoutine();
}

void riskyTactics() {     // risky, not tested yet
  /*
  * start with bot looking away from cherry basket
  * test code for team green, blue will be reversed
  */

  homingRoutine(2);

  waitForPullcord();

  driveTo(250, 1450);   // x1

  driveTo(660, 1450);   // x2

  turnTo(90);
  driveMaxUntilSwitch(true, CoAEnabled, true);
  driveDistanceMM(distanceDriveBackHolder+botLenght/2, 0, false, No, CoAEnabled);

  turnTo(0);            // x4
  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(fixSetPos(985, 0), 2850, 0);                        // Holder

  driveTo(660, 150);    // x6

  turnTo(0);            // x7
  driveMaxUntilSwitch(true, CoAEnabled, true);
  setPos(fixSetPos(0, 0), 150, 0);                          // test with default option
  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);

  preBasketRoutine();

  turnTo(270);          // x9
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  afterBasketRoutine();

  driveTo(700, distanceDriveBackHolder+botLenght/2);

  turnTo(0);
  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(985, 150, 0);                                       // Holder

  turnTo(180);
  driveMaxUntilSwitch(true, CoAEnabled, true);
  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);
  preBasketRoutine();
  
  turnTo(270);          // x12
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  afterBasketRoutine();

  driveTo(250, 1500);   // x13

  turnTo(180);          // x14
  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(fixSetPos(30, 180), 1450, 180);                     // Holder

  preBasketRoutine();

  turnTo(270);          // x17
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  afterBasketRoutine();

  endingRoutine();
}

void riskyTactics2() {    // our bot drives the same, big bot has differences
  riskyTactics();
}

void homeZoneTactics() {  // safe, working
  /*
  * start with bot looking towards cherry basket, start in end zone
  * test code for team green, blue will be reversed
  */

  homingRoutine(1);

  waitForPullcord();

  // start
  CoAEnabled = false;

  driveTo(700, distanceDriveBackHolder+botLenght/2);

  turnTo(0);            // x2
  CoAEnabled = CoADefault;
  ignoredUSdefault = L;
  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(fixSetPos(985, 0), 150);                            // Holder

  turnTo(180);

  ignoredUSdefault = R;
  driveMaxUntilSwitch(true, CoAEnabled, true);
  CoAEnabled = false;
  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);

  turnTo(270);          // x4
  preBasketRoutine();     
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  afterBasketRoutine();

  CoAEnabled = true;

  endingRoutine();
}

void preloadingTactics(bool DITEnabled) {// preload up to 7 cherries, working
  /**
   * @brief tactics for preloading up to 7 cherries
   * start with bot looking AWAY from basket
   * bot will start to drive until spontaneous placed cherry holder and pick up the cherries in it
   */

  CoAEnabled = false;

  //startup routine for preloading
  moveDown();
  delay(1000);
  closeGripper();
  delay(500);
  moveUp();       //has cherries in it

  //homing
  homingRoutine(1, false);  //only setting pos

  turnAngle(90, true); //turned to basket

  waitForPullcord();

  preBasketRoutine();

  driveMaxUntilSwitch(true, CoAEnabled, true);
  basketRoutine(225, fixSetPos(0, 270), 270);

  afterBasketRoutine();
  CoAEnabled = false;

  //begin

  if(DITEnabled) {
    turnTo(90);
    moveDown();
    driveDistanceMM(275, 0, true, No, CoAEnabled);
    liftArm();
    delay(500);
    lowerArm();
    moveUp();
    driveDistanceMM(275, 0, false, No, CoAEnabled);
  }


  // Holder 1
  driveTo(700, distanceDriveBackHolder+botLenght/2);
  turnTo(0);            // x2

  // ignoredUSdefault = L;

  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(fixSetPos(985, 0), 150);                            // Holder

  //basket wall
  turnTo(180);

  // ignoredUSdefault = R;

  driveMaxUntilSwitch(true, CoAEnabled, true);

  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);

  turnTo(270);          // x4
  preBasketRoutine();     
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  if(DITEnabled) detachServos();
  if(DITEnabled) while(true) {}

  afterBasketRoutine();

  ignoredUSdefault = R;
  CoAEnabled = CoADefault;

  //drive around cakes
  driveTo(450, 450);
  ignoredUSdefault = NONE;
  driveTo(450, 1000);

  // Holder 2
  driveTo(250, 1430);   // x2
  CoAEnabled = false;

  turnTo(180);          // x3                 // test with turnTo(), then use

  driveMaxUntilSwitch(true, CoAEnabled);

  holderRoutine(fixSetPos(53, 180), 1500, 180);                     // Holder, working

  CoAEnabled = CoADefault;

  driveTo(700, 300);    // x4
  CoAEnabled = false;

  //basket
  turnTo(180);          // x5
  driveMaxUntilSwitch(true, CoAEnabled, true);                // 180°
  setPos(fixSetPos(0, 180), 300, 180);                        // test with default option instead of "0"
  driveDistanceMM(distanceDriveBack, 0, false, No, CoAEnabled);

  turnTo(270);          // x7
  preBasketRoutine();
  driveMaxUntilSwitch(true, CoAEnabled);

  basketRoutine(225, fixSetPos(0, 270));                            // Basket

  //afterBasketRoutine();

  //endingRoutine();
}

void homologation() { //only used for homologation / CoA testing
  waitForPullcord();

  CoAEnabled = true;
  setPos(0, 0, 90);
  
  while(true) {
    driveTo(0, 500);
    driveTo(500, 500);
    driveTo(500, 0);
    driveTo(0, 0);
    turnTo(90);
  }
}