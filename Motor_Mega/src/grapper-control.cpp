#include <Arduino.h>
#include <Servo.h>

Servo Grapperleft;
Servo Grapperight;
Servo Gripperleft;
Servo Gripperright;
Servo Debug;

const int ServoSpeed = 15;

/*enum ServoObject {
  Graperleft,
  Graperright,
  Griperleft,
  Griperright,
}*/

/*void spinServo(ServoObject servo, int angle) {
  if (servo == Graperleft) {
    int oldangle;
    if(oldangle < angle) {
      for (pos = oldangle; pos <= angle; pos += 1) {
      Grapperleft.write(pos);              
      delay(ServoSpeed);                       
      }
      oldangle = angle;
    }
    else{
      for (pos = oldangle; pos <= angle; pos -= 1) {
        Grapperleft.write(pos);              
        delay(ServoSpeed);                       
      }
      oldangle = angle;
    }
  }
}*/
void debugServo(int angle) {
  Debug.write(angle);
}

void collectingposition() {
  Grapperleft.write(1);
  Grapperight.write(141);
}

void slotted() {
  Grapperleft.write(21);
  Grapperight.write(101);
}

void idle() {
  Grapperleft.write(121);
  Grapperight.write(1);
}

void Gripper_open() {
  Gripperleft.write(180);
  Gripperright.write(0);
}

void setup() {
  Grapperleft.attach(2);
  Grapperight.attach(3);
  Gripperleft.attach(6);
  Gripperright.attach(5);
  Debug.attach(4);
}

void loop() {
  debugServo(1);
}

 