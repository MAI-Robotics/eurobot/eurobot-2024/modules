#include <Arduino.h>
#include <ros.h>
#include <std_msgs/Int16.h>

// left dc motor pins
#define LEFT_DCM_LEFT_PWM 5
#define LEFT_DCM_RIGHT_PWM 4

// right dc motor pins
#define RIGHT_DCM_LEFT_PWM 2
#define RIGHT_DCM_RIGHT_PWM 3

const int leftMotorPin = 9;   // Pin für linken Motor
const int rightMotorPin = 10; // Pin für rechten Motor

char inputString[STRING_SIZE];
int outputArray[2];

void splitString()
{
  int i = 0;
  char *p = strtok(inputString, ";");

  while (p != NULL)
  {
    Serial.print("Section found: ");
    Serial.println(p);

    outputArray[i++] = atof(p);
    p = strtok(NULL, ";");
  }
}

void leftPWMCallback()
{
  int leftPWM = outputArray[0];
  analogWrite(leftMotorPin, abs(leftPWM));
  if (leftPWM >= 0)
  {
    analogWrite(LEFT_DCM_LEFT_PWM, 0);
    analogWrite(LEFT_DCM_RIGHT_PWM, leftPWM);
  }
  else
  {
    analogWrite(LEFT_DCM_RIGHT_PWM, leftPWM);
    analogWrite(LEFT_DCM_LEFT_PWM, 0);
  }
}

void rightPWMCallback()
{
  int rightPWM = outputArray[1];
  analogWrite(rightMotorPin, abs(rightPWM));
  if (rightPWM >= 0)
  {
    analogWrite(RIGHT_DCM_LEFT_PWM, 0);
    analogWrite(RIGHT_DCM_RIGHT_PWM, rightPWM);
  }
  else
  {
    analogWrite(RIGHT_DCM_RIGHT_PWM, rightPWM);
    analogWrite(RIGHT_DCM_LEFT_PWM, 0);
  }
}

void setup()
{
  Serial.begin(115200);

  pinMode(LEFT_DCM_LEFT_PWM, OUTPUT);
  pinMode(LEFT_DCM_RIGHT_PWM, OUTPUT);

  pinMode(RIGHT_DCM_LEFT_PWM, OUTPUT);
  pinMode(RIGHT_DCM_RIGHT_PWM, OUTPUT);

  pinMode(25, OUTPUT);
}

void loop()
{
  if (Serial.available() > 0)
  {
    Serial.readStringUntil('\n').toCharArray(inputString, STRING_SIZE);
    Serial.println(inputString);

    splitString();
    rightPWMCallback();
    leftPWMCallback();
  }

  delay(1);
}
