# How to Start Rosserial on Ros

1. Run ``roscore``

2. Start the rosseriel node with ``rosrun rosserial_python serial_node.py /dev/ttyUSB0``