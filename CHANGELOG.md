# Changelog of the "modules" repo
## Week 2
- started with encoders on Arduino Mega, connected to the RPI via rosserial
- done lidar connection on ROS 1
- will be starting with slam soon

## Week 3
- Rosserial done, communication working between mega and pi
- starting with navigation
