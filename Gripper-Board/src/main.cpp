#include <Arduino.h>
#include <Servo.h>

#define center_servo 4
#define left_servo 5
#define right_servo 6
#define front 7

Servo Center;
Servo Left;
Servo Right;
Servo Front_Grapper;

int currentangle;
int tempangle;
const int ServoSpeed = 20;

void setup() {
  Serial.begin(9600);
  Center.attach(center_servo);
  Left.attach(left_servo);
  Right.attach(right_servo);
  Front_Grapper.attach(front);
}

void ServoUp(int targetAngle) {
    for (int pos = currentangle; pos <= targetAngle; pos += 1) { 
    Center.write(pos); 
    tempangle = pos;            
    delay(ServoSpeed);  
  }
  currentangle = tempangle;        
}

void ServoDown(int targetAngle) {
  for (int pos = currentangle; pos >= targetAngle; pos -= 1) { 
    Center.write(pos); 
    tempangle = pos;            
    delay(ServoSpeed);
  }  
  currentangle = tempangle;
}

void loop() {
  //Front_Grapper.write(180);
  /*currentangle = 80;
  Center.write(currentangle);
  delay(1000);*/
  Center.write(130);
  delay(10000);
  Left.write(180);
  Right.write(60);
  delay(2000);
  Center.write(80);
  delay(1000);
  Left.write(60);
  Right.write(180);
  delay(1000);
  Center.write(130);
  Front_Grapper.write(90);
  delay(5000);
/*Front Servo, slotting: 180, hochgeklappt: 90
  Center Servo, greifen: 140, hochgeklappt 90+ NOCH ANPASSEN
  Left Servo, open: 90, closed: 180
  Right Servo, open: 180, closed 90*/
}

