import numpy as np
from rplidar import RPLidar

# Größe der Karte in Metern und Auflösung in Zellen pro Meter
MAP_WIDTH_M = 2
MAP_HEIGHT_M = 3
RESOLUTION = 10  # 1 Zelle pro 10 cm

# Größe der Karte in Zellen berechnen
MAP_WIDTH = int(MAP_WIDTH_M * RESOLUTION)
MAP_HEIGHT = int(MAP_HEIGHT_M * RESOLUTION)

# Erstelle eine leere Karte
map_data = np.zeros((MAP_HEIGHT, MAP_WIDTH), dtype=np.uint8)

# Funktion zur Konvertierung von Metern zu Kartenkoordinaten
def meters_to_map_coordinates(x, y):
    map_x = int(x * RESOLUTION)
    map_y = int(y * RESOLUTION)
    return map_x, map_y

# Verbindung zum RPLidar herstellen
lidar = RPLidar('/dev/ttyUSB0')  # Passe den Port entsprechend deiner Konfiguration an

try:
    # Starte das Lidar
    lidar.start_motor()

    # Beginne Daten zu sammeln
    for scan in lidar.iter_scans():
        # Lösche die Karte für den nächsten Scan
        map_data.fill(0)

        for (_, angle, distance) in scan:
            # Ignoriere Werte außerhalb des Messbereichs
            if distance > 0.01 and distance < 15:
                # Berechne die Position des Hindernisses in der Karte
                x = distance * np.cos(np.radians(angle))
                y = distance * np.sin(np.radians(angle))
                map_x, map_y = meters_to_map_coordinates(x, y)

                # Markiere das Hindernis in der Karte
                map_data[map_y, map_x] = 1

        # Hier kannst du die Karte ausgeben oder weiterverarbeiten
        print(map_data)

except KeyboardInterrupt:
    print("Programm wurde gestoppt")

finally:
    # Beende das Lidar und schließe die Verbindung
    lidar.stop_motor()
    lidar.disconnect()

