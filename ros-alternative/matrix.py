def create_matrix(rows, cols):
    matrix = [[0 for _ in range(cols)] for _ in range(rows)]
    return matrix

rows = 200
cols = 300

matrix = create_matrix(rows, cols)
print(matrix)

