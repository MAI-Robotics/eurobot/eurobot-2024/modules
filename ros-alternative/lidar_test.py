from rplidar import RPLidar

# Verbindung zum RPLidar herstellen
lidar = RPLidar('/dev/ttyUSB0')  # Passe den Port entsprechend deiner Konfiguration an

try:
    # Starte das Lidar
    lidar.start_motor()

    # Beginne Daten zu sammeln
    for scan in lidar.iter_scans():
        for (_, angle, distance) in scan:
            # Hier kannst du die Lidardaten verarbeiten
            print(f"Angle: {angle}, Distance: {distance}")

except KeyboardInterrupt:
    print("Programm wurde gestoppt")

finally:
    # Beende das Lidar und schließe die Verbindung
    lidar.stop_motor()
    lidar.disconnect()

