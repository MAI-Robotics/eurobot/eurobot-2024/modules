# to test: https://brean.github.io/svelte-pyscript-pathfinding

from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.a_star import AStarFinder

#matrix = [["0" for _ in range(16)] for _ in range(16)]
matrix = [[1 for _ in range(16)] for _ in range(16)]
# 0 oder kleiner: Hindernis
# Größer als null: Frei, je kleiner desto bevorzugter

grid = Grid(matrix=matrix)

start = grid.node(0, 0)
end = grid.node(15, 15)

finder = AStarFinder(diagonal_movement=DiagonalMovement.always)
    # Diagonal movement allowed
path, runs = finder.find_path(start, end, grid)
# Finder starten

print('operations:', runs, 'path length:', len(path))
print(grid.grid_str(path=path, start=start, end=end))
